var http = require('http');
module.exports = {};
module.exports.get = function(baseUrl, path, data, onSuccess, onError) {
    var instance = this;
    var stream = "";
    var coolDown = 10000;
    var options = { 
        host:  baseUrl,
        port: '80',
        path: path + data,
        method: 'GET'
    };
    var request = http.request(options, function(response) {
        response.on('data', function(chunk) {
            stream = stream + chunk;
        });
        response.on('end', function() {
            if(response.statusCode === 200) {
                onSuccess(stream, data);
            } else {
                //We got denied, the API hates request hammering
                console.log("Retrying");
                setTimeout(function() { 
                    instance.get(baseUrl, path, data, onSuccess, onError);
                        }, coolDown);
            }
        });
    });
    request.end();
    request.on('error', function(error) {
        console.log("GET error:" + error);
        console.log("Retrying");
        setTimeout(function() { 
            instance.get(baseUrl, path, data, onSuccess, onError);
        }, coolDown);
    });
};
