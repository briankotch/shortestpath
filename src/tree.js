module.exports = {
    map: {},
    backlinks: {},
    start: 0,
    goal: 0,
    //Walk the tree backwards from an ending node to a beginning node
    //Follow the ancestors with the lowest depth
    calculateShortestPath: function(beginning, end, walker) {
        walker = walker || {"history": [], "found": false};
        beginning = parseInt(beginning);
        end = parseInt(end);
        walker.history.unshift(end);
        //We are the alpha and the omega
        //Also, we found the top
        if(beginning === end)  {
            walker.found = true;
            return walker;
        }
        var end_node = this.map[end];
        var shortest_parent = 0;
        var shortest_depth = Object.keys(this.map).length;
        //for each parent node associated with the end node we're looking at
        //find the parent with the shortest depth from the top (beginning node);
        for(var i = 0; i < this.map[end].parents.length; i++) {
            var parent_location = this.map[end].parents[i];
            if(this.map[parent_location].depth < shortest_depth) {
                shortest_depth = this.map[parent_location].depth;
                shortest_parent = parent_location;
            }
        }
        //If we're not at the top, keep looking, but use the shortest_parent as our new end point
        return this.calculateShortestPath(beginning, shortest_parent, walker);
    },
    //explore all possible branches to see if we can get from beginning to the end
    //if a back link is encountered, the history is updated, but the link is not followed
    calculatePath: function(beginning, end, walker) {
        var instance = this;
        if (walker === undefined) {
            walker = {'queue':[],'history':{},log: [], 'found': false };
            walker.log.push("Searching for " + beginning + " to " + end);
            walker.history[beginning] = true;
        }
        //look ahead on our map, for new nodes or collisions;
        this.map[beginning].children.forEach(
            function(child) {
                if (child == end) {
                    walker.log.push("Found " + end);
                    walker.found = true;
                    return walker;
                }
                
                if (walker.history[child] === undefined && instance.map[child].depth <= instance.map[end].depth)
                {
                    walker.history[child] = true;
                    walker.queue.push(child);
                }
            }
        );
        if(walker.queue.length === 0) {
            return walker;
        }
        var explorer = walker.queue.pop();
        walker.log.push("Exploring " + explorer);
        return this.calculatePath(explorer, end, walker);
    },

    //For each collision detected while building the map
    //Check to see if the collision's parent leads back to itself
    //By jumping back into the tree
    findDirectedCycles: function() {
        var directedCycles = 0;
        var backlinks = Object.keys(this.backlinks);
        var instance = this;
        //We need to make sure that the back link is a loop
        //If the parent and the child are on the same branch
        //we should be able to find a path
        //calculatePath does not follow backlinks and returns the moment it finds a match
        backlinks.forEach(
            function(backlink) {
                instance.backlinks[backlink].forEach(
                    function(parent) {
                        if(instance.map[parent] !== undefined) {
                            var walker = instance.calculatePath(parent, parent);
                            if (walker.found) {
                                console.log(walker);
                                directedCycles = directedCycles + 1;
                            }
                        }
                    }
                );
            }
        );
        return directedCycles;
    },
    // location: the id of the new node
    // parent: the id of the node to attach it too
    addNode: function(location, parent) {
        var depth = 0;
        //If we are not at the start of the tree, add the location to the parent
        if(parent !== 0) {
            this.map[parent].children.push(location);
            depth = this.map[parent].depth;
        } else {
            //set the first node!
            this.start = location;
        }
        if(this.map[location] === undefined) {
            this.map[location] = {};
            this.map[location].parents = [];
            this.map[location].children = [];
            this.map[location].parents.push(parent);
            this.map[location].depth = depth + 1;
        } else {
            this.addBacklink(location, parent);
        }
    },
    // location: the id of an existing node;
    // parent: the id of parent to create a link too
    // this creates symlink between two nodes (ie, does not create a new node);
    addBacklink: function(location, parent) {
        if(this.backlinks[location] === undefined) { 
            this.backlinks[location] = this.map[location].parents.slice(0); 
        }
        this.map[parent].children.push(location);
        this.map[location].parents.push(parent);
        this.backlinks[location].push(parent);
    }
};
