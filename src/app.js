//Self executing main module
(function() {
    //back reference for sane callbacks
    var instance = this;
    //module for processing location maths
    var parser = require("./parser.js");
    //Encapsulation of an HTTP guest request with some special tracking for our purposes
    var requester = require("./request.js");
    //url pieces
    var baseUrl = "www.crunchyroll.com";
    var path = "/tech-challenge/roaming-math/briankotch@gmail.com/";
    //Fs, for writing our results
    var fs = require('fs');
    //The beginning point specified by crunchy role. Obviously, we should parameterize this
    var url ="abs(add(subtract(subtract(multiply(-1,13558),190),232),subtract(199,add(3,multiply(255,40599)))))";

    //Tree mapping object
    this.tree = require("./tree.js");
    //counter to keep track of the traversals in progress
    this.searches = 0;
    //Recursive success function passed into the Requester object
    //Reponse is either
    //GOAL, meaning it's the one we're looking for
    //DEADEND, the terminal node
    //MOAR, more location maths separated by carriage returns
    //Note, this traverses the entire tree, visiting every node it finds
    this.onSuccess = function(response, location) {
        instance.searches = instance.searches - 1;
        //Huzzah, we found a terminal goal node. This will be important once we finish mapping our ... map
        if(response === "GOAL") {
            instance.tree.map[location].status = 'goal';
            instance.tree.goal = location;
        } else if (response === "DEADEND") {
            //BOO, a dead end. 
            instance.tree.map[location].status = 'deadend';
        } else {
            this.linkPages = this.linkPages + 1;
            //We have more to do!
            //split the maths and recurse!
            var destinations = response.split('\n');
            //if we haven't found our goal, examine the branches         
            for(var i = 0; i < destinations.length; i = i + 1) {
              instance.getLocation(destinations[i], location);  
            }
        }
        //If the counter reaches 0, then all callbacks have fired.
        if(instance.searches === 0) {
            console.log(instance.tree.map);
            instance.done();
            return;
        }
    };

    //This is fired when success has processed all the results
    this.done = function() {
        console.log("Jobs done");
        var answer = {};
        answer.goal = instance.tree.goal;
        answer.directed_cycle_count = instance.tree.findDirectedCycles();
        answer.node_count = Object.keys(instance.tree.map).length;
        var walker = instance.tree.calculateShortestPath(instance.tree.start, instance.tree.goal);
        answer.shortest_path = walker.history;
        var output = JSON.stringify(answer);
        console.log(output);
        fs.writeFile("output.js", output, function() {});
        output = JSON.stringify(instance.tree.map);
        fs.writeFile("map.js", output, function() {});
        output = JSON.stringify(instance.tree.backlinks);
        fs.writeFile("backlinks.js", output, function() {});
    };

    this.getLocation = function(startingUrl, parent) {
        var location = parser.parse(startingUrl);
        //if our location does not exist, go get it.
        if(instance.tree.map[location] === undefined) {
            instance.tree.addNode(location, parent);
            instance.searches = instance.searches + 1;
            console.log("Fetching " + location);
            //retrieve the information about the location
            requester.get(baseUrl, path, location, instance.onSuccess);
        } else { 
            instance.tree.addNode(location, parent);
        }
    };
    this.getLocation(url, 0);
} ());
