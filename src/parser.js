module.exports  =  {
    //You can haz scalable functions, just add to this collection before you call parse!
    //params: an array of values to do math on
    //return a parsedFloat
    commands: {
        'add': function(params) {
            return parseInt(params[0]) + parseFloat(params[1]);
        },
        'subtract': function(params) {
            return parseInt(params[0]) - parseFloat(params[1]);
        },
        'multiply': function(params) {
            return parseInt(params[0]) * parseFloat(params[1]);
        },
        'abs': function(params) {
            return Math.abs(parseFloat(params[0]));
        }
    },
    
    //No regex, no looping by hand. Find the first closing parenthesis
    //Then find its values and operation
    //Replace the calculated value and recurse!
    //Do so until no input resolves to a number
    //This function assumes clean, well-formed input. Scrub your strings, close your parens
    //Could add trim for spaces and to lower case for commands
    //input: a string of parenthetical expressions in the format of operator(x,y);
    //    where operators are functions defined in the commands collection 
    //    add(1+multiply(1,1));
    parse: function(input) {
        //Find the deepest () not requiring substitution
        //Find the first closed parenthesis for our search
        var closingP = input.indexOf(')');
        //Find the matching beginning parathesis
        var beginning = input.substring(0, closingP).lastIndexOf('(');
        if (closingP === -1) {
            return parseInt(input);
        }
        //turn the stuff inside into an array, at this point either n or n,n
        var parameters = input.substring(beginning + 1, closingP).split(',');
        //This is safe because of Node's EC5 guarantee. 
        var keys = Object.keys(this.commands);
        var parsed = '';
        var value = 0;
        //For each command in the collection, check the key 
        for (var i = 0; i < keys.length; i++) {
            //extract the operator from the found, deepest ()
            //match it against the keys of our mapped function
            var command = input.substring(beginning - keys[i].length, beginning);
            if (command === keys[i]) {
                value = this.commands[keys[i]](parameters);
                //Everything before the value to be replaced
                parsed = input.substring(0, beginning - keys[i].length);
                parsed = parsed + value + input.substring(closingP + 1);
                break;
            }
        }
        //If we are not a number, then we have done all the maths
        if (isNaN(parsed)) {
            return this.parse(parsed);
        } else {
            return parseInt(value);
        }
    }
    
};

