Tree problem
===
I did this test for Crunchyroll.

Seriously, folks. You expect a UI designer to use a recursive descent parser, crazy URL schemes and THEN a shortest path algorithm? Ok. Here you go. It's written in node, built by Grunt and completely unit tested. Not full coverage, but definitely all the use cases I cared to think of.

Deployment
===
npm install

Testing
===
grunt

Running 
===
node src/app.js

What it does
===
It turns
---
`abs(add(subtract(subtract(multiply(-1,13558),190),232),subtract(199,add(3,multiply(255,40599)))))`

From
---
[Their test page](http://www.crunchyroll.com/tech-challenge/roaming-math/briankotch@gmail.com/10366529)

Into
---
`{"goal":293182,"node_count":41,"shortest_path":[10366529,16378854,8597371,293182],"directed_cycle_count":3}`

What it demonstrates: mostly, recursion. I really like recursion.
---
The first section is a recursive parser. The format was guaranteed santizied, so there is no real syntax checking. It finds the first closing ) (closeP) of a parenthetical expression and then scans back until it finds a valid expression. Then it simplifies and replaces the formula with the value. 

The second part is following the links. Since this function generates n requests where n is the number of unique nodes, it easily clocked their server's api request limit. So, it has an auto fail rety mechanism that cools down when it gets a not 200 status code.

The third part is building and parsing a node tree.

It consists of: 
* a starting node referenced by the given equation.
* a tree of nodes ending in:
** DEADEND 
** GOAL
** A list of more links. 
** Some links are cyclical
** Some links jump back to a different part of the tree
** Some links are infinite looks by jumping back to an arbitrary child node

To build the tree, we add a node by coordinates. The value of the node and it's parent. If it does not have a parent, it is set to zero. The first node added becomes our starting node. All nodes are given a depth of their parent's depth + 1. Each subsequent node is added with a value and a reference to its parent. We maintain a hash of node values. If the child node already exists, we have a back link. Each node tracks its children and its parents. Back links are noted.

To parse the tree, we need to examine it and walk it.

`tree.calculateShortestPath(node, node)`

Walks the tree from child to parent, following the parents with the shortest depth. It assumes the nodes are connected. 

`tree.calculatePath(node, node)`

Walks the tree in a greedy and exhaustive manner. All nodes encountered are added to its queue. It will touch them all once until it finds them.

The exhaustive list of nodes that must be visited is equal total number of distinct nodes.
