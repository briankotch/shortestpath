var tree = require("../src/tree.js");
describe("tree", function() {
    it("Should exist", function() {
        expect(tree).toBeDefined();
    });
    it("Should contain a map", function() { 
        expect(tree.map).toBeDefined();
    });
 });
describe("Structure tests", function() {
    /*
              
      0 - 1 - 2 - 4 - 5
            - 3 - 1 (backlink)
            - 6 - 7
     */
    it("Adds a location", function() { 
        tree.addNode(1, 0);
        expect(tree.map[1]).toBeDefined();
        expect(tree.map[1].depth).toEqual(1);
        //top of the tree);
        expect(tree.map[1].parents[0]).toEqual(0);
        var treeSize = Object.keys(tree.map).length;
        expect(treeSize).toEqual(1);
    });
    it("Adds a second location", function() {
        tree.addNode(2,1);
        expect(tree.map[2]).toBeDefined();
        expect(tree.map[2].depth).toEqual(2);
        expect(tree.map[2].parents[0]).toEqual(1);
        var treeSize = Object.keys(tree.map).length;
        expect(treeSize).toEqual(2);
    });
    it("Adds a sibling", function() {
        tree.addNode(3,1);
        expect(tree.map[3]).toBeDefined();
        expect(tree.map[3].depth).toEqual(2);
        expect(tree.map[3].parents[0]).toEqual(1);
        var treeSize = Object.keys(tree.map).length;
        expect(treeSize).toEqual(3);
    });
    it("Adds a backlink", function() {
        tree.addNode(1,3);
        var backLinkCount = Object.keys(tree.backlinks).length;
        expect(backLinkCount).toEqual(1);
    });
    it("Finds the shortest path", function() {
        tree.addNode(4,2);
        tree.addNode(5,4);
        //This one includes a back link
        //Three goes directly to one
        var locations = tree.calculateShortestPath(1, 3);
        expect(locations.history).toContain(1);
        expect(locations.history).toContain(3)
        expect(locations.found).toBeTruthy();
        locations = tree.calculateShortestPath(1, 5);
        expect(locations.history).toContain(1);
        expect(locations.history).toContain(2);
        expect(locations.history).toContain(4);
        expect(locations.history).toContain(5);
    });
    //Test our ability to find nodes from beginning to end
    it("Walk the path", function() {
        //This path should exist
        var walker = tree.calculatePath(1, 5);
        expect(walker.found).toBeTruthy();
        //Add a new branch
        tree.addNode(6, 1);
        tree.addNode(7, 6);
        //This path should not exist
        walker = tree.calculatePath(5, 7);
        expect(walker.found).toBeFalsy();

    });
    if("Should not find the shortest path", function() {
        var walker = tree.calculateShortestPath(5, 7);
        expect(tree.walker.found).toBeFalsy();
    });
    it("Detects Directed Cycles", function() {
        expect(tree.findDirectedCycles()).toEqual(1);
    });
});
