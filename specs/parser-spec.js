var parser = require("../src/parser.js");
describe("parse", function() {
    it("Should exist", function() {
        expect(parser).toBeDefined();
    });
});
describe("Handle returning a number", function() {
    it("Should return 4 from '4'", function() {
        var result = parser.parse("4");
        expect(result).toEqual(4);
    });
    it("Should return 4 from '4.0'", function() {
        var result = parser.parse("4.0");
        expect(result).toEqual(4);
    });
    it("Should return 4 from '4.01'", function() {
        var result = parser.parse('4.01');
        expect(result).toEqual(4);
    });
    it("Should add 1 + 1", function() {
        var result = parser.parse('add(1,1)');
        expect(result).toEqual(2);
    });
    it("Should subtract 1 - 1", function() {
        var result = parser.parse('subtract(1,1)');
        expect(result).toEqual(0);
    });
    it("Should multiply 1 * 1", function() {
        var result = parser.parse('multiply(1,1)');
        expect(result).toEqual(1);
    });
    it("Should abs |-1]", function() {
        var result = parser.parse('abs(-1)');
        expect(result).toEqual(1);
    });
    it("Should solve 3(1+1)", function() {
        var result = parser.parse('multiply(3,add(1,1))');
        expect(result).toEqual(6);
    });
    it("Should solve (3*2)+(5*6)", function() {
        var result = parser.parse('add(multiply(3,2),multiply(5,6))');
        expect(result).toEqual(36);
    });
});
